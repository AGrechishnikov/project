package com.application.core;

import java.time.LocalDate;

public class FineInfo {
    private int id;
    private String carID;
    private String officerName;
    private String violatorName;
    private Double fineSum;
    private boolean finePaid;
    private boolean needCourt;
    private LocalDate protocolDate;
    private LocalDate fineDeadlineDate;
    private LocalDate paymentDate;

    void setCarID(String id){
        this.carID=id;
    }

    void setOfficerName(String name){
        this.officerName=name;
    }

    public String getCarID() {
        return carID;
    }

    public String getOfficerName() {
        return officerName;
    }

    public String getViolatorName() {
        return violatorName;
    }

    public void setViolatorName(String violatorName) {
        this.violatorName = violatorName;
    }

    public Double getFineSum() {
        return fineSum;
    }

    public void setFineSum(Double fineSum) {
        this.fineSum = fineSum;
    }

    public boolean isFinePaid() {
        return finePaid;
    }

    public void setFinePaid(boolean finePaid) {
        this.finePaid = finePaid;
    }

    public boolean isNeedCourt() {
        return needCourt;
    }

    public void setNeedCourt(boolean needCourt) {
        this.needCourt = needCourt;
    }

    public LocalDate getProtocolDate() {
        return protocolDate;
    }

    public void setProtocolDate(LocalDate protocolDate) {
        this.protocolDate = protocolDate;
    }

    public LocalDate getFineDeadlineDate() {
        return fineDeadlineDate;
    }

    public void setFineDeadlineDate(LocalDate fineDeadlineDate) {
        this.fineDeadlineDate = fineDeadlineDate;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }
    public int getId(){
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
