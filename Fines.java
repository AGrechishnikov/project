package com.application.core;

import java.util.ArrayList;
import java.util.HashMap;

public class Fines {
    public ArrayList<FineInfo> finesList;
    public Fines(){
        finesList=new ArrayList<>();
    }
    public void setFineWithID(FineInfo editedFine){
        for (FineInfo fine:finesList) {
            if(fine.getId()==editedFine.getId()){
                fine=editedFine;
            }
        }
    }

}
