package com.application.core.controllers;

import com.application.core.FineInfo;
import com.application.core.Fines;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

@Controller
@RequestMapping("/api/v1/fines")
public class AppController {
    Fines database;
    AppController(){
        database=new Fines();
    }

    @GetMapping
    public ArrayList<FineInfo> getAll(){
            return database.finesList;
    }
    @PostMapping
    public int postFine(@RequestBody FineInfo fine){
        database.finesList.add(fine);
        return fine.getId();
    }
    @PutMapping
    public String editFine(@RequestBody FineInfo editedFine)
    {
        database.setFineWithID(editedFine);
        return "Fine id " + editedFine.getId() + " edited";
    }
    @DeleteMapping
    public String deleteFine(@RequestBody FineInfo fineToDelete){
        for (FineInfo fine: database.finesList) {
            if(fine==fineToDelete){
                database.finesList.remove(fine);
            }
        }
        return "Deleted" + fineToDelete;
    }
    @PatchMapping(value = "/{id}/pay")
    public String pay(@PathVariable("id") int id) {
        for (FineInfo fine: database.finesList) {
            if(fine.getId()==id){
                fine.setFinePaid(true);
                fine.setPaymentDate(LocalDate.now());
                return "ID " + fine.getId()+" payment has been paid";
            }
        }
        return "ID "+ id + " wasn't found";
    }

    @PatchMapping(value = "/{id}/court")
    public String court(@PathVariable("id") int id) {
        for (FineInfo fine : database.finesList) {
            if(fine.getId()==id){
                fine.setNeedCourt(true);
                return "ID " + fine.getId() + " needs court";
            }
        }
        return "ID "+id+" wasn't found";
    }
}
